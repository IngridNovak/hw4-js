    "use strict";
//1.Функції дозволяють писати код що можна повторно використовувати, абстрагувати, присвоїти йому ім'я і за необхідності викликати.
    //Дозволяє вносити зміни в одному місці. Функція може бути анонімна, може повертати значення, "замикатись".
    //Крім того, ми псотійно використовуємо функції в коді (console.log,alert,prompt - вбудовані функції).
//2.Аргументи передаються викликаючи функцію. Вони зіставляються з параметрами, як аргумент можна передати будь яке значення (рядок, число,
    //логічне значення). Параметри визначаються один раз, але аргументи можна призначати різні.
//3.Return повертає значення або вираз в результаті виклику функції. Return виходить з функції і повертає значення про результат виконання функції.
let firstNumber = +prompt('Введіть перше число:');
let secondNumber = +prompt('Введіть друге число:');
let operators = prompt('Введіть оператор:');

while(isNaN(firstNumber) || !firstNumber || isNaN(secondNumber) || !secondNumber){
    firstNumber = +prompt('Введіть перше число:',firstNumber);
    secondNumber = +prompt('Введіть друге число:',secondNumber);
    operators = prompt('Введіть оператор:',operators);
}

function calculate(firstNum,secondNum,operator){
    switch(operator){
        case "+":
            return firstNum + secondNum;

        case "-":
            return firstNum - secondNum;

        case "*":
            return firstNum * secondNum;

        case "/":
            return firstNum / secondNum;

    }
}
console.log(calculate(firstNumber,secondNumber,operators));